from rest_framework import serializers

from .models import User
from .forms import AuthenticationForm


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'email', 'is_staff', 'is_active',]


class LoginSerializer(serializers.Serializer):
    email = serializers.EmailField()
    password = serializers.CharField()

    def validate(self, data):
        form = AuthenticationForm(request=self.context['request'], data=data)
        if not form.is_valid():
            raise serializers.ValidationError(form.errors)
        self.user = form.user
        return data

    def create(self, validated_data):
        validated_data.update({
            'created_by': self.context['request'].user
        })
        return super().create(validated_data)
