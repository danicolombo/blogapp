from rest_framework import serializers

from users.serializers import UserSerializer
from .models import Post, Topic


class TopicSerializer(serializers.ModelSerializer):
    class Meta:
        model = Topic
        fields = ['id', 'name']


class PostSerializer(serializers.ModelSerializer):
    created_by = UserSerializer(read_only=True)

    class Meta:
        model = Post
        fields = ['id', 'title', 'description', 'content', 'image', 'created', 'created_by', 'topic']
