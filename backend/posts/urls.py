from rest_framework import routers

from django.urls import include, path

from .views import PostViewSet, TopicViewSet


router = routers.DefaultRouter()
router.register(r'posts', PostViewSet)
router.register(r'topics', TopicViewSet)

urlpatterns = [
    path('', include(router.urls)),
]

