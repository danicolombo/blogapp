# Generated by Django 3.1.5 on 2021-05-10 02:31

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import model_utils.fields
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Topic',
            fields=[
                ('id', model_utils.fields.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=250, verbose_name='name')),
            ],
            options={
                'verbose_name': ['topic'],
                'verbose_name_plural': 'topics',
            },
        ),
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', model_utils.fields.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('title', models.CharField(max_length=250, verbose_name='title')),
                ('content', models.TextField(max_length=1250, verbose_name='content')),
                ('description', models.TextField(max_length=350, verbose_name='description')),
                ('image', models.ImageField(blank=True, default='', null=True, upload_to='images/', verbose_name='image')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('created_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='posts', to=settings.AUTH_USER_MODEL, verbose_name='created by')),
                ('topic', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='posts', to='posts.topic', verbose_name='topic')),
            ],
            options={
                'verbose_name': ['post'],
                'verbose_name_plural': 'posts',
                'ordering': ['-created'],
            },
        ),
    ]
