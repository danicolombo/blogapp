from django.contrib import admin
from .models import Post, Topic


admin.site.register(Topic)


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'description', 'content', 'image', 'created', 'created_by', 'topic']
    search_fields = [
        'id',
        'title__unaccent__icontains',
        'description__unaccent__icontains',
        'content__unaccent__icontains',
    ]

    def get_user_display(self, obj):
        return str(obj.created_by)
    get_user_display.short_description = 'created_by'
