from django.db import models
from django.contrib.auth.models import User

from model_utils.models import UUIDModel
from model_utils.fields import AutoCreatedField


class Post(UUIDModel):
    title = models.CharField('title', max_length=250)
    content = models.TextField('content', max_length=1250)
    description = models.TextField('description', max_length=350)
    image = models.ImageField('image', upload_to='images/', default='', blank=True, null=True)
    created = AutoCreatedField('created')
    created_by = models.ForeignKey('users.User', models.SET_NULL, related_name='posts', null=True, blank=True, verbose_name='created by')
    topic = models.ForeignKey('posts.Topic', models.SET_NULL, related_name='posts', null=True, blank=True, verbose_name='topic')
    
    class Meta:
        verbose_name = ['post']
        verbose_name_plural = ('posts')
        ordering = ['-created']

    def __str__(self):
        return self.title


class Topic(UUIDModel):
    name = models.CharField('name', max_length=250)

    class Meta:
        verbose_name = ['topic']
        verbose_name_plural = ('topics')

    def __str__(self):
        return self.name
