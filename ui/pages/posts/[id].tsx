import React, {useEffect, useState} from 'react'
import {SITE_NAME} from '../../src/types/constants'
import {api} from '../../src/api'
import {PostResponse} from '../../src/types/posts'
import {Box, Card, CardMedia, CircularProgress, Grid, Typography } from '@material-ui/core'
import {NextPage} from 'next'
import {useRouter} from 'next/router'
import {PageHeading} from '../../src/components/PageHeading'
import Head from 'next/head'
import TopBar from '../../src/components/TopBar'
import styles from '../../styles/Slug.module.css'


const PostDetail: NextPage = () => {
    const router = useRouter()
    const id = router.query.id as string
    const [loading, setLoading] = useState(true)
    const [post, setPost] = useState<PostResponse>()

    useEffect(() => {
        const fetchPost = async (id: string) => {
            const post = await api.getPost(id)
            setPost(post)
            setLoading(false)
        }
        fetchPost(id)
    }, [])

    return (
        <>
            <TopBar />
            {loading 
            ? <CircularProgress color='primary' />
            : <Box>
                {post && (
                    <>
                        <Head>
                            <title>{post.title}</title>
                            <meta name="description" content={post.description} />
                            <meta property="article:published_time" content={post.created} />
                            <meta name="twitter:card" content="summary" />
                            <meta name="twitter:description" content={post.description} />
                            <meta name="twitter:title" content={post.title} />
                            <meta property="og:type" content="article" />
                            <meta property="og:title" content={post.title} />
                            <meta property="og:description" content={post.description} />
                            <meta property="og:site_name" content={SITE_NAME} />
                        </Head>
                        <Grid container justify="center" alignItems="center">
                            <Grid item md={6} xs={12} className={styles.headings}>
                            <PageHeading title={post.title} />
                            </Grid>
                        </Grid>
                        <Grid container justify="center" alignItems="center">
                            <Grid item md={6} xs={12} className={styles.info}>
                                {post.created && (
                                    <Typography variant="body1">
                                        {post.created.split('T')[0]} — written by{' '}
                                        <a
                                            target="_blank"
                                            rel="noreferrer"
                                            style={{
                                                display: 'inline-flex',
                                                alignItems: 'center',
                                            }}
                                        >
                                            {post.created_by.email}
                                        </a>
                                    </Typography>
                                )}
                            </Grid>
                        </Grid>
                        <Grid container justify="center" alignItems="center">
                            <Grid item md={6} xs={12} className={styles.info}>
                                <Card className={styles.image}>
                                    <CardMedia className={styles.image} image={post.image ? post.image : '/images/default.jpg'} title={post.title} />
                                </Card>
                                <Typography variant="body1" className={styles.content}> 
                                    {post.content}
                                </Typography>
                            </Grid>
                        </Grid>
                        <Grid container justify="center" alignItems="center">
                            <Grid item md={6} xs={12} className={styles.info}>
                                <Typography variant="body1">
                                    Contact: {' '}
                                    <a
                                        target="_blank"
                                        rel="noreferrer"
                                        style={{
                                            display: 'inline-flex',
                                            alignItems: 'center',
                                        }}
                                    >
                                        {post.created_by.email}
                                    </a>
                                </Typography>
                            </Grid>
                        </Grid>
                    </>
                )}
            </Box>
            }
        </>
    )
}

export default PostDetail
