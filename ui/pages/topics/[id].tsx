import React, {useEffect, useState} from 'react'
import {SITE_NAME} from '../../src/types/constants'
import {NextPage} from 'next'
import {api} from '../../src/api'
import {PostsResponse, Topic} from '../../src/types/posts'
import {Chip, Box, CircularProgress, Grid, makeStyles, Typography} from '@material-ui/core'
import {Preview} from '../../src/components/Preview'
import {useRouter} from 'next/router'
import Head from 'next/head'
import TopBar from '../../src/components/TopBar'
import styles from '../../styles/Shared.module.css'


const useStyles = makeStyles((theme) => ({
    container: {
        display: 'flex',
        justifyContent: 'center',
        flexWrap: 'wrap',
        '& > *': {
            margin: theme.spacing(0.5),
        },
    },
    chip: {
        color: '#444452',
    }
}))

const Topics: NextPage = () => {
    const [loading, setLoading] = useState(true)
    const [posts, setPosts] = useState<PostsResponse>()
    const [currentTopic, setCurrentTopic] = useState<string>('')
    const [topics, setTopics] = useState<Topic[]>([])
    const router = useRouter()
    const classes = useStyles()
    const topic = router.query.id as string

    useEffect(() => {
        const fetchPosts = async (topic: string) => {
            const posts = await api.getPosts(topic)
            setPosts(posts)
        }
        const fetchTopics = async () => {
            const topics = await api.getTopics()
            setTopics(topics)
            setCurrentTopic(topics?.find(t => t.id === topic).name)
        }
        fetchPosts(topic)
        fetchTopics()
        setLoading(false)
    }, [topic])

    return (
        <>
            <Head>
                <title>{SITE_NAME}</title>
                <meta
                    name="description"
                    content={`${SITE_NAME} is an open source project developed with react, nextJS, typescript + Django Rest Framework`}
                />
            </Head>
            <TopBar />
            <Grid container>
                <Grid item xs={12} className={styles.headings}>
                    <Box p={5}>
                        <Typography variant='h2'>{currentTopic}</Typography>
                    </Box>
                </Grid>
                {loading
                    ? <CircularProgress color='primary' />
                    : <>
                        <Grid container item xs={12} className={classes.container} justify='center' spacing={2}>
                            {topics?.map(t => (
                                <Chip
                                    key={`${t.id}`}
                                    label={t.name}
                                    className={classes.chip}
                                    variant="outlined"
                                    onClick={() => router.push(`/topics/${t.id}`)} />
                            ))}
                        </Grid>
                        <Grid item xs={12}>
                            <Box pt={3}>
                                <Preview posts={posts} />
                            </Box>
                        </Grid>
                    </>
                }
            </Grid>
        </>
    )
}

export default Topics
