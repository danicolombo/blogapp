const withImages = require('next-images');

module.exports = withImages({
  async rewrites() {
    return [
      {
        source: '/admin',
        destination: 'http://localhost:8000/:path*',
      }
    ]
  },
  webpack(config) {
    config.module.rules.push({
      test: /\.svg$/,
      issuer: {
        test: /\.(js|ts)x?$/,
      },
      use: ['@svgr/webpack'],
    });

    return config;
  },
})