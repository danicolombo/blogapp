import {useRouter} from 'next/router'
import {api} from '../api'
import {ComponentType, createContext, FC, useContext, useEffect, useState} from 'react'
import {LoginResponse} from '../types/users'
import * as settings from '../types/constants'


type AuthContextType = {
    authUser: LoginResponse | undefined
    setAuthUser(user: LoginResponse | undefined): void
}

const AuthContext = createContext<AuthContextType>({
    authUser: undefined,
    setAuthUser: () => {},
})

export const AuthProvider: FC = ({ children }) => {
    const [user, setUser] = useState<LoginResponse | undefined>(undefined)
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        const getActiveUser = async () => {
            try {
                const user = await api.activeUser()
                setUser(user)
            } catch {
                // noop
            }
            setLoading(false)
        }
        getActiveUser()
    }, [])

    return (
        <AuthContext.Provider value={{ authUser: user, setAuthUser: setUser }}>
            {children}
        </AuthContext.Provider>
    )
}

export const useAuth = () => useContext(AuthContext)

type UserCheck = (user: LoginResponse) => boolean

export const userCheckRequired = (check: UserCheck) => (Component: ComponentType) => () => {
    const router = useRouter()
    const { authUser } = useAuth()

    useEffect(() => {
        const tokenStorage = window.localStorage.getItem('token')
        if (!authUser?.token && tokenStorage === '') router.push(`${settings.LOGIN_URL}?next=${router.pathname}`)
        else if (!check(authUser)) console.log(authUser)
    }, [authUser])

    return authUser ? <Component /> : null
}

export const loginRequired = userCheckRequired(user => Boolean(user))
