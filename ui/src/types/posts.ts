import { User } from "./users"

export type Topic = {
    id: string
    name: string
}

export type Post = {
    id: string
    content: string
    created: string
    title: string
    description: string
    image: string
    created_by: User
    topic: Topic
}

export type PostResponse = Post

export type PostsResponse = Post[]

export type TopicsResponse = Topic[]

export type PostCreateRequest = {
    title: string
    description: string
    content: string
    image: File
    topic: string
}

export type PostRequest = {
    formData: any
}

