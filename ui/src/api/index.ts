import {WEB_SERVICE} from '../types/constants'
import {PostResponse, PostsResponse, TopicsResponse} from '../types/posts'
import {LoginRequest, LoginResponse} from '../types/users'

class API {
    async fetch<T>(
        method: string,
        url: string,
        body: any = undefined,
    ): Promise<T> {

        const valuesToSend = body

        if (body?.image){
            let formData = new FormData()

            formData.append('title', body.title)
            formData.append('description', body.description)
            formData.append('content', body.content)
            formData.append('topic', body.topic)
            formData.append('image', body.image)
            body = formData
        }

        try {
            if(method == 'post' && url != 'users/login/'){
            const token = window.localStorage.getItem('token')
            //const response = await fetch(`/api/${url}`, {
            const response = await fetch(`${WEB_SERVICE}/api/${url}`, {
                method,
                credentials: 'same-origin',
                headers: valuesToSend?.image ? {
                    'Accept': 'application/json', 
                    'type': 'formData', 
                    'Authorization': `token ${token}`
                } : {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json', 
                    'Authorization': `token ${token}`
                },
                body: body && !valuesToSend?.image
                    ? JSON.stringify(body)
                    : body,
            })

            if (response.status === 204) return {} as T
            if (response.status === 404) return {} as T
            
            else {
                const responseBody = await response.json()
                return responseBody
            }
            }else{
                const response = await fetch(`${WEB_SERVICE}/api/${url}`, {
                method,
                credentials: 'same-origin',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json', 
                },
                body: body && !body?.image
                    ? JSON.stringify(body)
                    : body,
            })

            if (response.status === 204) return {} as T
            if (response.status === 404) return {} as T
            
            else {
                const responseBody = await response.json()
                return responseBody
            }
            }

        } catch (error) {
            console.error(error)
        }
    }

    async activeUser() {
        return this.fetch<LoginResponse>('get', 'users/active-user' )
    }

    async login(body: LoginRequest) {
        return this.fetch<LoginResponse>('post', 'users/login/', body)
    }

    async logout()  {
        return this.fetch<{}>('post', 'users/logout/')
    }

    async getPosts(topic?: string) {
        return this.fetch<PostsResponse>('get', topic ? `posts/?topic=${topic}` : 'posts/')
    }

    async getPost(id: string) {
        return this.fetch<PostResponse>('get', `posts/${id}`)
    }

    async getTopics() {
        return this.fetch<TopicsResponse>('get', 'topics/' )
    }

    async createPost(body) {
        return this.fetch<PostResponse>('post', 'posts/', body)
    }

}

export const api = new API()
