import {createMuiTheme, ThemeOptions} from '@material-ui/core'

export const paletteColorsDark = {
    primary: '#444452',
    secondary: '#ffe0ac',
    error: '#E44C65',
    background: '#353540', 
    text: '#FFF',
}

export const paletteColorsLight = {
    primary: '#FFF',
    secondary: '#00bff3',
    error: '#00bcf1',
    background: '#FFF',
    text: '#444452',
}

const options = (dark: boolean): ThemeOptions => {
    const paletteColors = dark ? paletteColorsDark : paletteColorsLight
    return {
        palette: {
            type: dark ? 'dark' : 'light',
            primary: {
                main: paletteColors.primary,
            },
            secondary: {
                main: paletteColors.secondary,
            },
            error: {
                main: paletteColors.error,
            },
            background: {
                default: paletteColors.background,
            },
            text: {
                primary: paletteColors.text,
            },
        },
        typography: {
            fontFamily: ['super-black', 'super-bold', 'super-regular', 'super-medium', 'Lato', 'Roboto'].join(','),
            h1: {
            fontFamily: '"super-black", Lato',
                    fontWeight: 700,
                    fontSize: '96px',
                    lineHeight: '127px',
                    letterSpacing: '-1.5px',
            },
            h2: { fontFamily: 'Elderkin', fontSize: '60px', textTransform: 'uppercase'},
            h3: { fontFamily: 'Roboto Slab', fontSize: '48px', lineHeight: '63px' },
            h4: { fontFamily: 'Roboto Slab', fontSize: '20px',    fontWeight: 700, lineHeight: '26px', letterSpacing: '0.25px', marginBottom: '10px' },
            h5: { fontFamily: 'Lato', fontSize: '20px',    fontWeight: 700, lineHeight: '26px', letterSpacing: '0.25px' },
            h6: { fontFamily: 'Miller-regular', fontWeight: 500, fontSize: '20px', lineHeight: '26px', letterSpacing: '0.15px' },
            subtitle1: { fontFamily: 'Elderkin', fontSize: '20px', fontWeight: 700, paddingBottom: '10px', lineHeight: '19px', letterSpacing: '0.15px' },
            subtitle2: {
                fontFamily: 'Elderkin',
                fontWeight: 500,
                fontSize: '18px',
                lineHeight: '16.41px',
                letterSpacing: '0.1px',
                textTransform: 'uppercase',
            },
            body1: { fontFamily: 'Lato', fontSize: '18px', lineHeight: '200%', letterSpacing: '0.5px' },
            body2: { fontFamily: 'Miller-regular', fontSize: '14px',    fontWeight: 700, lineHeight: '16px', letterSpacing: '0.25px' },
            button: { fontFamily: 'Lato', fontWeight: 500, fontSize: '14px', letterSpacing: '1.25px' },
            caption: { fontFamily: 'Lato', fontSize: '12px', lineHeight: '14px', letterSpacing: '0.4px' },
            overline: { fontFamily: 'Elderkin', fontSize: '25px', textTransform: 'uppercase', letterSpacing: '1.5px'},
        },
        overrides: {
            MuiCssBaseline: {
                '@global': {
                    html: {
                        height: '100%',
                        padding: 0,
                        margin: 0,
                        width: '100vw',
                    },
                    body: {
                        height: '100%',
                        padding: 0,
                        margin: 0,
                        width: '100vw',
                        overflowX: 'hidden',
                    },
                    a: {
                        textDecoration: 'none',
                        fontWeight: 900,
                        color: paletteColors.text,
                    },
                },
            },
        },
    }
}

export const darkTheme = createMuiTheme(options(true))
export const lightTheme = createMuiTheme(options(false))

export default darkTheme
