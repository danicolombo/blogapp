import React, {FC} from 'react'
import {Grid, GridProps, TextField, TextFieldProps} from '@material-ui/core'
import {FastField, FieldProps} from 'formik'


type BaseTextInputProps = {
    name: string;
}

export type FormChild = {
    gridProps?: GridProps | false;
}

export const defaultGridProps: GridProps = {
    xs: 12,
    sm: 6,
}

export type TextInputProps = FormChild
    & BaseTextInputProps
    & Pick<TextFieldProps, 'label' | 'disabled' | 'placeholder' | 'multiline' | 'rows' | 'type' | 'inputMode' | 'size' | 'className'>


export const TextInput: FC<TextInputProps> = ({
    name,
    disabled,
    inputMode,
    gridProps,
    ...otherProps
}) => {
    const component = (
        <FastField name={name}>
            {({ field, form, meta }: FieldProps) => (
                <TextField
                    variant='outlined'
                    fullWidth={true}
                    disabled={form.isSubmitting || disabled}
                    error={meta.touched && Boolean(meta.error)}
                    helperText={meta.touched && meta.error}
                    inputProps={{ inputMode }}
                    {...field}
                    {...otherProps}
                />
            )}
        </FastField>
    );

    if (!gridProps) return component;

    return (
        <Grid item={true} {...gridProps}>
            {component}
        </Grid>
    );
};

TextInput.defaultProps = {
    gridProps: defaultGridProps,
};
