import {FC} from 'react'
import {Grid} from '@material-ui/core'
import {Alert} from '@material-ui/lab'
import {useFormikContext} from 'formik'


export const FormErrors: FC = () => {
    const { status } = useFormikContext()

    if (!status?.formErrors) return null

    return (
        <Grid item={true} xs={12}>
            <Alert severity='error'>
                {status.formErrors}
            </Alert>
        </Grid>
    );
};
