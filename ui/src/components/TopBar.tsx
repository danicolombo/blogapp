import React, {ReactElement, useContext} from 'react'
import {NAME_AND_DOMAIN} from '../types/constants'
import {Button, Grid, Typography, Box, useMediaQuery, Tooltip, List, ListItem, Link } from '@material-ui/core'
import {MenuCustom} from '../../src/components/Menu'
import {ToggleThemeContext} from '../theme'
import SunIcon from '@material-ui/icons/WbSunnyOutlined'
import MoonIcon from '@material-ui/icons/Brightness2Outlined'
import styles from '../../styles/Shared.module.css'


export const TopBar = (): ReactElement => {
    const {toggleTheme, isDark} = useContext(ToggleThemeContext)
    const large = useMediaQuery('(min-width:700px)')

    return (
        <Grid container>
            <Grid item xs={1} className={styles.headings}>
                <MenuCustom />
            </Grid>
            <Grid item lg={2} className={styles.laterallinks}>
                <List component="nav" aria-label="main folders" className={styles.tinylinksContainer}>
                    <ListItem button className={styles.listitem}>
                        <Typography variant='body2' className={styles.tinylinks}>
                            A collaborative blog
                        </Typography>
                    </ListItem>
                    <ListItem button className={styles.listitem}>
                        <Typography variant='body2' className={styles.tinylinks}>
                            for tiny  and friendly posts
                        </Typography>
                    </ListItem>
                    <ListItem button className={styles.listitem}>
                        <Typography variant='body2' className={styles.tinylinks}>
                            about technology
                        </Typography>
                    </ListItem>
                </List>
            </Grid>
            <Grid item xs={10} md={10} lg={6} className={styles.headings}>
                <Box>
                    <Typography variant={large ? 'h1' : 'h4'}>{NAME_AND_DOMAIN}</Typography>
                    <div className={styles.containerlogo} >
                        <img className={styles.logokas} src={'/images/logokas.png'} />
                    </div>
                </Box>
            </Grid>
            <Grid item lg={2} className={styles.laterallinks}>
                <List component="nav" aria-label="main folders" className={styles.tinylinksContainer}>
                    <ListItem button className={styles.listitem}>
                        <Typography variant='body2' className={styles.tinylinks}>
                            A technical challenge
                        </Typography>
                    </ListItem>
                    <ListItem button className={styles.listitem}>
                        <Typography variant='body2' className={styles.tinylinks}>
                            for kas factory built
                        </Typography>
                    </ListItem>
                    <ListItem button className={styles.listitem}>
                        <Typography variant='body2' className={styles.tinylinks}>
                            with Django + React
                        </Typography>
                    </ListItem>
                </List>
            </Grid>
            <Grid item xs={1} className={styles.headings}>
                <Tooltip title="Theme color">
                    <Button variant="text" color="inherit" onClick={toggleTheme}>
                        {isDark ? <SunIcon /> : <MoonIcon />}
                    </Button>
                </Tooltip>
            </Grid>
        </Grid>
    )
}

export default TopBar
