
import React, { FC, ReactElement } from 'react'
import { Typography } from '@material-ui/core'
import style from '../../styles/Shared.module.css'
import { Button, MenuItem, makeStyles, Menu } from '@material-ui/core'
import { useRouter } from 'next/router'

const useStyles = makeStyles((theme) => ({
    drop: {
        '& .MuiList-root':{
            maxHeight: '710px',
            border: '1px solid #bdbdbd',
            width: '375px',
        }
    },
    photoButton: {
        marginRight: '10px',
    },
    button: {
        margin: '10px 0px',
    }
}))




export const MenuCustom: FC = ({ }): ReactElement => {
    const router = useRouter()
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
    const classes = useStyles()
    const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <div>    
            <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick} className={style.menu}>
                    <span className={style.hamburger}>
                        <span className={style.inactive}>
                        <svg className={style.menus} role="presentation" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 34 34">
                                <path className={style.menus}  d="M18.35 1l-2.99 2.91-3.66-2.02-1.55 3.88-4.16-.36.17 4.16-3.96 1.36 1.86 3.74L1 17.52l3.22 2.66-1.64 3.84 4.03 1.13.07 4.17 4.14-.61 1.77 3.78 3.53-2.22L19.28 33l2.32-3.47 4 1.22.7-4.11 4.15-.51-1.03-4.04L33 19.94l-2.59-3.27 2.4-3.42-3.71-1.94.8-4.1-4.17-.26-.94-4.07-3.93 1.45L18.35 1z"></path>
                                <path className={style.menusvg} d="M22.84 13.42H11.13"></path>
                                <path className={style.menusvg} d="M22.84 21.42H11.13"></path>
                                <path className={style.menusvg} d="M22.84 17.42H11.13"></path>
                            </svg>
                        </span>
                    </span>
            </Button>
                <Menu
                    id="simple-menu"
                    anchorEl={anchorEl}
                    keepMounted
                    className={classes.drop}
                    open={Boolean(anchorEl)}
                    onClose={handleClose}
                >
                <MenuItem onClick={() => router.push('/')}>HOME</MenuItem>
                <MenuItem onClick={() => router.push('/posts')}>POSTS</MenuItem>
                    <br />
                    <MenuItem onClick={handleClose}>React, React Hooks</MenuItem>
                    <MenuItem onClick={handleClose}>NextJS, Typescript, Material UI</MenuItem>
                    <MenuItem onClick={handleClose}>NodeJS, Docker</MenuItem>
                    <MenuItem onClick={handleClose}>Django + DRF</MenuItem>
                    <MenuItem onClick={handleClose}>
                        <div className={style.social}>
                            <div className={style.services} data-editable="services">
                            <button type="button" className={style.follow_circle} onClick={() => router.push("https://www.linkedin.com/in/decolombo/")}>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm-2 16h-2v-6h2v6zm-1-6.891c-.607 0-1.1-.496-1.1-1.109 0-.612.492-1.109 1.1-1.109s1.1.497 1.1 1.109c0 .613-.493 1.109-1.1 1.109zm8 6.891h-1.998v-2.861c0-1.881-2.002-1.722-2.002 0v2.861h-2v-6h2v1.093c.872-1.616 4-1.736 4 1.548v3.359z" /></svg>
                            </button>
                            <button type="button" className={style.follow_circle} onClick={() => router.push("https://procnedc.github.io/resume/")}>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm1 16.057v-3.057h2.994c-.059 1.143-.212 2.24-.456 3.279-.823-.12-1.674-.188-2.538-.222zm1.957 2.162c-.499 1.33-1.159 2.497-1.957 3.456v-3.62c.666.028 1.319.081 1.957.164zm-1.957-7.219v-3.015c.868-.034 1.721-.103 2.548-.224.238 1.027.389 2.111.446 3.239h-2.994zm0-5.014v-3.661c.806.969 1.471 2.15 1.971 3.496-.642.084-1.3.137-1.971.165zm2.703-3.267c1.237.496 2.354 1.228 3.29 2.146-.642.234-1.311.442-2.019.607-.344-.992-.775-1.91-1.271-2.753zm-7.241 13.56c-.244-1.039-.398-2.136-.456-3.279h2.994v3.057c-.865.034-1.714.102-2.538.222zm2.538 1.776v3.62c-.798-.959-1.458-2.126-1.957-3.456.638-.083 1.291-.136 1.957-.164zm-2.994-7.055c.057-1.128.207-2.212.446-3.239.827.121 1.68.19 2.548.224v3.015h-2.994zm1.024-5.179c.5-1.346 1.165-2.527 1.97-3.496v3.661c-.671-.028-1.329-.081-1.97-.165zm-2.005-.35c-.708-.165-1.377-.373-2.018-.607.937-.918 2.053-1.65 3.29-2.146-.496.844-.927 1.762-1.272 2.753zm-.549 1.918c-.264 1.151-.434 2.36-.492 3.611h-3.933c.165-1.658.739-3.197 1.617-4.518.88.361 1.816.67 2.808.907zm.009 9.262c-.988.236-1.92.542-2.797.9-.89-1.328-1.471-2.879-1.637-4.551h3.934c.058 1.265.231 2.488.5 3.651zm.553 1.917c.342.976.768 1.881 1.257 2.712-1.223-.49-2.326-1.211-3.256-2.115.636-.229 1.299-.435 1.999-.597zm9.924 0c.7.163 1.362.367 1.999.597-.931.903-2.034 1.625-3.257 2.116.489-.832.915-1.737 1.258-2.713zm.553-1.917c.27-1.163.442-2.386.501-3.651h3.934c-.167 1.672-.748 3.223-1.638 4.551-.877-.358-1.81-.664-2.797-.9zm.501-5.651c-.058-1.251-.229-2.46-.492-3.611.992-.237 1.929-.546 2.809-.907.877 1.321 1.451 2.86 1.616 4.518h-3.933z" /></svg>
                            </button>
                            </div>
                        </div>
                    </MenuItem>
                </Menu>
        </div>
    )
}




