import React, {FC, ReactElement} from 'react'
import {useRouter} from 'next/router'
import {Post} from '../types/posts'
import {Box, Card, CardActionArea, CardContent, CardMedia, Button, Typography, CardActions, makeStyles} from '@material-ui/core'
import ChevronRight from '@material-ui/icons/ChevronRight'


type PreviewCardProps = {
  post: Post
  noMargin?: boolean
}

const useStyles = makeStyles((theme) => ({
    media: {
        height: '400px',
        '&:hover': {
            border: "20px solid #00bff3",
            opacity: '0.7',
        },
    },
    root: {
        minWidth: 275,
        boxShadow: 'unset !important',
        borderBottom: '4px double #111',
    },
    container: {
        border: '1px solid grey',
        padding: '3px 15px',
        background: 'white',
        position: 'absolute',
        bottom: '-10px',
        left: '36%',
        color: 'black',
    },
    actions: {
        justifyContent: 'flex-end',
    }
}))

export const HomePostCard: FC<PreviewCardProps> = ({ post, noMargin }): ReactElement => {
    const classes = useStyles()
    const router = useRouter()

    return (
        <Card className={classes.root} onClick={() => router.push('/posts/[id]', `/posts/${post.id as string}`)}>
            <CardActionArea>
            <CardMedia className={classes.media} image={post.image ? post.image : '/images/default.jpg'} title={post.title} />
                <Typography variant="h5" component="h2" className={classes.container} >
                    Tiny Posts
                </Typography>
            </CardActionArea>
            <CardContent>
                <Typography gutterBottom variant="h2" component="h2">
                    {post.title}
                </Typography>
            <Typography variant="h4" color="textPrimary" component="h4">
                {post.description}
            </Typography>
            </CardContent>
            <CardActions className={classes.actions}>
            <Box pt={3}>
                <Button size="small" variant="text">
                    Read More <ChevronRight style={{ marginLeft: 20 }} />
                </Button>
            </Box>
            </CardActions>
        </Card>
    )
}

export default HomePostCard
