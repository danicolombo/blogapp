import React, {FC, ReactElement} from 'react'
import {makeStyles} from '@material-ui/core/styles'
import {Box, Card, CardActionArea, CardContent, CardMedia, Button, Typography, CardActions} from '@material-ui/core'
import ChevronRight from '@material-ui/icons/ChevronRight'
import {useRouter} from 'next/router'
import {Post} from '../types/posts'


type PreviewCardProps = {
    post: Post
    noMargin?: boolean
}

const useStyles = makeStyles((theme) => ({
    card: {
        width: 400,
        height: 430,
        margin: 'auto',
        position: 'relative',
        cursor: 'pointer',
        backgroundColor: theme.palette.primary.light,
        color: theme.palette.text.secondary,
    },
    media: {
        height: 160,
    },
    actions: {
        right: 20,
        bottom: 10,
        padding: 10,
        position: 'absolute',
    },
}))
export const PreviewCard: FC<PreviewCardProps> = ({ post, noMargin }): ReactElement => {
    const classes = useStyles()
    const router = useRouter()

    return (
        <Card className={classes.card} elevation={3} style={noMargin ? { margin: 0 } : {}} onClick={() => router.push('/posts/[id]', `/posts/${post.id as string}`)}>
            <CardActionArea>
                <CardMedia className={classes.media} image={post.image ? post.image : '/images/default.jpg'} title={post.title} />
            </CardActionArea>
            <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                    {post.title}
                </Typography>
                <Typography variant="subtitle1" color="textPrimary" component="p">
                    {post.description}
                </Typography>
            </CardContent>
            <CardActions>
                <Box pt={3}>
                    <Button size="small" variant="text" className={classes.actions}>
                        Read More <ChevronRight style={{ marginLeft: 20 }} />
                    </Button>
                </Box>
            </CardActions>
        </Card>
    )
}

export default PreviewCard
