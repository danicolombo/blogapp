# BlogApp

> A webapp for kasfactory challenge

It's an open source project developed with react, typescript, material ui, nextJS and Django + DRF.

Make sure to have Cors enable in your browser.

## Build

Inside the ui directory, install the dependencies (with Node version major 14):

```
$ npm install
```
and execute
```
$ npm run dev
```
or
```
$ node dev-server.js
```

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

Inside the backend directory:

Create the virtualenv and activate it
```
virtualenv .
```
```
source bin/activate
```

Install the requirements

```
pip install -r requirements.txt
```

Run the migrations
```
python manage.py makemigrations
```
```
python manage.py migrate
```

Create your superuser
```
python manage.py createsuperuser
```

To populate with some users and posts:
```
python manage.py loaddata src/fixtures/data.json
```

Run the django backend
```
python manage.py runserver
```

## Design

It was developed with the following libraries:

* "typescript"
* "@material-ui/core"
* "@material-ui/icons"
* "@material-ui/lab"
* "nodeJS"
* "formik"

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

* "react": "17.0.2"

Fonts used: [Roboto][0] & GT-Super.

## [MIT][1] License 

Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
and associated documentation files (the "Software"), to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
NCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


[0]: https://fonts.google.com/specimen/Roboto
[1]: https://opensource.org/licenses/MIT
[2]: http://www.omdbapi.com
